#!/usr/bin/perl
#
#
use utf8;
use JSON;
use Data::Dumper;

my $json = '';
my %categories;

while(<>) {
   $json .= $_;
}
if (1) {
    local $/;
    open( my $fh, "<", '/home/feitingen/.categories.json');
    $json_text = <$fh>;
    $cat = decode_json($json_text);
    %categories = %{$cat};
}


my @uncat;
my @entries = @{ from_json($json) };
#exit 0;
#print "!Type:Bank\n";
foreach $entry (@entries) {
        #    print Dumper $entry;
    $entry->{"description"} =~ s/\n/ - /g;
    foreach my $cat(keys %categories) {
        my $line = $entry->{"description"};
        if ($line =~ /$cat/i) {
            $entry->{"category"} = $categories{$cat};
        }
    }
    unless (defined $entry->{"category"}) {
        push @uncat, $entry->{"description"};
            $entry->{"category"} = "Misc";
    }
    $entry->{"description"} =~ s/\n/ - /g;
    $v = 0;
    $v += $entry->{'debit'}  if defined $entry->{'debit'};
    $v -= $entry->{'credit'} if defined $entry->{'credit'};
    print "D".$entry->{'date'}."\nM".$entry->{"description"}
          ."\nT".$v
          ."\nL".$entry->{"category"}
          ."\n^\n";
}
print STDERR to_json( \@uncat, {utf8 => 1, pretty => 1});

