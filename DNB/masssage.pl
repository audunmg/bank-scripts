#!/usr/bin/perl -w
use Data::Dumper;
#use Encode;
use utf8;
#use encoding 'utf8', Filter => 1;
use JSON;
binmode STDIN, ':encoding(UTF-8)'; 
#binmode STDOUT, ':encoding(UTF-8)';

my %categories;
if (1) {
    local $/;
    open( my $fh, "<", '/home/feitingen/.categories.json');
    $json_text = <$fh>;
    $cat = decode_json($json_text);
    %categories = %{$cat};
}


print "!Account\nNAssets:Current Assets:DNB:VISA\nTBank\n^\n!Type:Bank\n";
foreach my $file (@ARGV) {
  open FILE, ($file) or die "ERROR: $!";
  while (<FILE>) {
    #binmode FILE, ':encoding(UTF-8)'; 
    chomp;
    s/\r//g;
    #utf8::decode($_);
    #utf8::encode($_);
    #$_ = decode('ISO-8859-1', $_, Encode::FB_CROAK);
    #$_ = encode('UTF-8', $_, Encode::FB_CROAK);
    s/^"//;
    s/"$//;
    my ($dato, $forklaring, $rentedato, $uttak, $innskudd) =split /";"/;
    
    my $category = "Miscellaneous";
    foreach my $cat (keys %categories) {
      utf8::encode($cat);
      if ($forklaring =~ /$cat/i) {
            utf8::decode($cat);
        $category = $categories{$cat}; 
      }
    }
    if ($category eq "Miscellaneous") { 
      print STDERR "Uncategorized: '$forklaring'\n"
    }
    my $payer;
    #my $currency = 'nok'
    my $number;
    if ($forklaring =~ /^Varekjøp (.*) Dato (.*)/) {
      $payer = $1;
    }
    if ($forklaring =~ /^Lønn \d+ (.*)/) {
      $payer = $1;
    }
    if ($forklaring =~ /^Visa  \d+ (.*)/) {
      my $visa = $1;
      if ($visa =~ /^Nok \d+,\d+ (.*)/) {
        $payer = $1;
      } elsif ($visa =~ s/(...) (\d+,\d+) (.*)Valutakurs: ([\d,]+)//) {
        $payer = $3;
        # $currency = $3; $amount = $2; $exchangerate = $4;
      } else {
        $payer = $visa;
      }
      $payer =~ s/^Norwegian a \d+/Norwegian A\/S/;
    }
    $uttak = 0 if ($uttak eq '');
    $innskudd = 0 if ($innskudd eq '');
    $uttak =~ s/\.//g;
    $uttak =~ s/,/\./g;
    $innskudd =~ s/\.//g;
    $innskudd =~ s/,/\./g;
    $amount = $innskudd - $uttak;
    
    $,="\t"; 
    #print Dumper \@a;
    print "D$dato\nM$forklaring\nT$amount\n";
    print "P$payer\n" if (defined $payer);
    print "L$category\n";
    print "N$number\n" if (defined $number);
    print "^\n";
  }
  close FILE
}
