#!/usr/bin/perl -w
use Data::Dumper;
use Encode;

my %categories = (
"^Omkostninger" => "Bank Service Charge",
"^Giro  \\d+ Telenor Norge As, Mobil" => "Phone",
"Cafe Abel Vestgrensa" => "Entertainment",
"Bunnpris" => "Groceries",
"Narvesen" => "Recreation",

);




foreach my $file (@ARGV) {
  open FILE, ($file) or die "ERROR: $!";
  while (<FILE>) {
    chomp;
    s/\r//g;

    $_ = decode('ISO-8859-1', $_, Encode::FB_CROAK);
    $_ = encode('UTF-8', $_, Encode::FB_CROAK);
    s/^"//;
    s/"$//;
    @a=split /";"/;
    $a[3] = "0,00" if $a[3] eq "";
    $a[4] = "0,00" unless defined $a[4];
    push @a, "VISA";
    my $category = "Miscellaneous";
    foreach my $cat (keys %categories) {
      if ($a[1] =~ /$cat/) {
        $category = $categories{$cat}; 
      }
    }
    push @a, $category;
    $a[2] =~ s/\./-/g;
    $a[0] =~ s/\./-/g;
    $,="\t"; 
    #print Dumper \@a;
    print @a;
    print "\n";
  }
  close FILE
}
